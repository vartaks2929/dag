"""Dynamically create DAGs based on the YAML file."""
import os
import yaml
# Short-term fix to avoid dependency conflict between dbt/airflow
# TO-DO: Set up virtual environment either for dbt or airflow
# pylint: disable=import-error
from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import PythonOperator
from airflow.utils.dates import datetime
from airflow.models.variable import Variable
from airflow.models import DagBag
from airflow.models import DagRun
from airflow.utils.state import State



DBT_DIR = '/opt/airflow/dags/repo/dbt_folder'
dag_target = '--profile testing_red --profiles-dir .'

def create_dag(dag: dict):
    """Creates DAG for a DBT job.

    Args:
        dag (dict): Attributes for a DBT job.
    """
    default_args = {
        'owner': dag['owner'],
        'start_date': datetime.strptime(dag['start_date'], '%Y-%m-%d')
    }
    schedule_interval = None
    tags = dag.get('tags')


    with DAG(
        dag_id=dag['dag_id'], default_args=default_args, description=dag['description'],
        schedule_interval=schedule_interval, catchup=False, tags=tags
    ) as dbt_dag:
        start_dag = DummyOperator(
            task_id='start_dag'
        )

        upstream_task = start_dag

        for command in dag['commands']:
            command['task_id'] = BashOperator(
                task_id=command['task_id'],
                bash_command=f'{command["command"]}',
                do_xcom_push=False
            )

            # pylint is treating this as a pointless statement but it is required to build the DAG
            # pylint: disable=pointless-statement
            upstream_task >> command['task_id']

            upstream_task = command['task_id']

    return dbt_dag

with open('/opt/airflow/dags/repo/dag_folder/dev_dbt_jobs.yml') as dags:
    DAGS = yaml.load(dags, Loader=yaml.BaseLoader)

for DAG_YML in DAGS['dags']:
    dag_id = DAG_YML['dag_id']

    chk_airflow_var = Variable.get(dag_id, deserialize_json=True, default_var=0)
    if chk_airflow_var==0:
        Variable.set(key=dag_id, value=DAG_YML, serialize_json=True)
    
    active_run = DagRun.find(dag_id=dag_id,state=State.RUNNING)
    # active_run will be empty if not active dag is running
    if not active_run: # meaning dag is not running, when dag is not running update the variable
        Variable.set(key=dag_id, value=DAG_YML, serialize_json=True)
    
    globals()[dag_id] = create_dag(dag=Variable.get(dag_id, deserialize_json=True))

def backup_original_main_saurabh():

    with open('/opt/airflow/dags/repo/dag_folder/dev_dbt_jobs.yml') as dags:
        DAGS = yaml.load(dags, Loader=yaml.BaseLoader)

    for DAG_YML in DAGS['dags']:
        dag_id = DAG_YML['dag_id']

        chk_airflow_var = Variable.get(dag_id, deserialize_json=True, default_var=0)
        if chk_airflow_var==0:
            Variable.set(key=dag_id, value=DAG_YML, serialize_json=True)
        
        active_run = DagRun.find(dag_id=dag_id,state=State.RUNNING)
        # active_run will be empty if not active dag is running
        if not active_run: # meaning dag is not running, when dag is not running update the variable
            Variable.set(key=dag_id, value=DAG_YML, serialize_json=True)
        
        globals()[dag_id] = create_dag(dag=Variable.get(dag_id, deserialize_json=True))
